package com.company;

import java.awt.Color;

import java.awt.Graphics;

public class Paddle {
    public int NumberPaddle;

    public int x,y, width = 20,
                    height = 140;

    public int score;

    public Paddle (Pong pong, int NumberPaddle) {


        this.NumberPaddle = NumberPaddle;

        if (NumberPaddle == 1){
            this.x = 10;
        }

        if (NumberPaddle == 2){
            this.x = pong.width - width - 10;
        }

        this.y = pong.height / 2 - this.height / 2;
    }

    public void render (Graphics g){
        g.setColor(Color.BLUE);
        g.fillRect(x, y, width, height);
    }

    public void move(boolean up){
        int speed = 6;

        if (up) {
            if (y - speed > 0){
                y -= speed;
            }

            else {
                y = 0;
            }
        } else {

            if (y + height + speed < Pong.pong.height) {

                y += speed;
            }

            else {

                y = Pong.pong.height - height;
            }
        }
    }
}

